// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]

daftarHewan.sort() //sort array dengan fungsi sort()
for(var i = 0; i < daftarHewan.length; i++){
    console.log(daftarHewan[i]) //menampilkan isi array
}

// soal 2
function introduce(data){
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby //jawaban soal 2
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)

console.log(perkenalan) 

// soal 3
function hitung_huruf_vokal(kata){
    var vokal = "aiueoAIUEO"    // deklarasi huruf vokal, kecil dan kapital
    var jumlah = 0              // deklarasi untuk penyimpanan jumlah huruf vokal, dimulai dari 0
    for(var i = 0; i < kata.length; i++){        //looping kata yang akan dihitung huruf vokalnya
        for(var j = 0; j < vokal.length; j++){   //looping huruf vokal untuk mencari nilai yang sama
            if(kata[i] == vokal[j]){    // jika kata ke-i memuat vokal ke-j
                jumlah++                // maka jumlah ditambah 1
            }
        }
    }
    return jumlah
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// soal 4
function hitung(angka) {
    return angka*2 - 2 //jawaban soal 4
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
