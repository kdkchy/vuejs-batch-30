//quiz 1
function jumlah_kata(data){
    var kata_arr = data.split(" ")
    var hasil = 0
    for(var i = 1; i < kata_arr.length; i++) {
        if(kata_arr[i] != ''){
            hasil++
        }
    } 
    console.log(hasil)
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4


//quiz 2
function next_date(tanggal, bulan, tahun) {
    
    var str_bulan = bulan_str(bulan)[0]
    var tanggal_akhir = bulan_str(bulan)[1]
    
    if(bulan == 12 && tanggal_akhir == 31) {
        var tahun = tahun+1
    } else {
        var tahun = tahun
    }

    if (tanggal<1) {
        tanggal_akhir = 'ups, tanggalnya salah'
        str_bulan = ''
        tahun = ''
    } 
    else if (tanggal>tanggal_akhir) {
        tanggal_akhir = 'ups, tanggalnya salah'
        str_bulan = ''
        tahun = ''
    } 
    else if(tanggal<tanggal_akhir){
        tanggal_akhir = tanggal+1
    }
    else if(tanggal==tanggal_akhir){
        tanggal_akhir = 1
        if(bulan == 12){
            var str_bulan = bulan_str(1)[0]
        } else {
            var str_bulan = bulan_str(bulan+1)[0]
        }
        
    } 

    console.log(tanggal_akhir + ' ' + str_bulan + ' ' + tahun)
}

function bulan_str(bulan) {
    switch(bulan) {
        case 1: {
            str_bulan = 'Januari'
            tanggal_akhir = 31
            break
        }
        case 2: { 
            str_bulan = 'Februari'
            if(tahun%4==0){
                tanggal_akhir = 29
            } else {
                tanggal_akhir = 28
            }
            break
        }
        case 3: {
            str_bulan = 'Maret'
            tanggal_akhir = 31
            break
        }
        case 4: { 
            str_bulan = 'April'
            tanggal_akhir = 30
            break
        }
        case 5: { 
            str_bulan = 'Mei'
            tanggal_akhir = 31
            break
        }
        case 6: { 
            str_bulan = 'Juni'
            tanggal_akhir = 30
            break
        }
        case 7: { 
            str_bulan = 'Juli'
            tanggal_akhir = 31
            break
        }
        case 8: { 
            str_bulan = 'Agustus'
            tanggal_akhir = 31
            break
        }
        case 9: { 
            str_bulan = 'September'
            tanggal_akhir = 30
            break
        }
        case 10: { 
            str_bulan = 'Oktober'
            tanggal_akhir = 31
            break
        }
        case 11: { 
            str_bulan = 'November'
            tanggal_akhir = 30
            break
        }
        case 12: { 
            str_bulan = 'Desember'
            tanggal_akhir = 31
            break
        } 
        default: {
            str_bulan = 'ups, bulannya salah'
            tanggal_akhir = ''
        }
    }

    return [str_bulan, tanggal_akhir]
}


var tanggal = 31
var bulan = 11
var tahun = 2021

next_date(tanggal, bulan, tahun)