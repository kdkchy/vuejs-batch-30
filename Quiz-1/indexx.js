function next_date(tanggal, bulan, tahun) {
    
    var bulan_str_arr = [
        {
            'index'         : 0,
            'str'           : 'ups, bulan salah',
            'tanggal_akhir' : ''
        },
        {
            'index'         : 1,
            'str'           : 'Januari',
            'tanggal_akhir' : 31
        },
        {
            'index'         : 2,
            'str'           : 'Februari',
            'tanggal_akhir' : 28,
            'tanggal_kabisat' : 29
        },
        {
            'index'         : 3,
            'str'           : 'Maret',
            'tanggal_akhir' : 31
        },
        {
            'index'         : 4,
            'str'           : 'April',
            'tanggal_akhir' : 30
        },
        {
            'index'         : 5,
            'str'           : 'Mei',
            'tanggal_akhir' : 31
        },
        {
            'index'         : 6,
            'str'           : 'Juni',
            'tanggal_akhir' : 30
        },
        {
            'index'         : 7,
            'str'           : 'Juli',
            'tanggal_akhir' : 31
        },
        {
            'index'         : 8,
            'str'           : 'Agustus',
            'tanggal_akhir' : 31
        },
        {
            'index'         : 9,
            'str'           : 'September',
            'tanggal_akhir' : 30
        },
        {
            'index'         : 10,
            'str'           : 'Oktober',
            'tanggal_akhir' : 31
        },
        {
            'index'         : 11,
            'str'           : 'November',
            'tanggal_akhir' : 30
        },
        {
            'index'         : 12,
            'str'           : 'Desember',
            'tanggal_akhir' : 31
        },
        {
            'str'           : 'ups, bulannya salah',
            'tanggal_akhir' : ''
        }
    ]

    var str_bulan = ''
    var tanggal = tanggal
    var tahun = tahun
    var tanggal_akhir = bulan_str_arr[bulan].tanggal_akhir

    if(tahun % 4 == 0 && bulan == 2) {
        tanggal_akhir = bulan_str_arr[bulan].tanggal_kabisat
    } 

    if(bulan == 12 && tanggal_akhir == 31){
        tahun = tahun+1
    } 

    if(tanggal==tanggal_akhir) {
        if(bulan == 12){
            str_bulan = bulan_str_arr[1].str
        } else if(bulan >12) {
            str_bulan = 'ups, bulannya salah'
        } else {
            str_bulan = bulan_str_arr[bulan+1].str
        }
        tanggal = 1   
    } else if (tanggal < 1 || tanggal > tanggal_akhir || bulan > 12) {
        tanggal = 'ups, tanggalnya salah'
    }

    console.log(tanggal + ' ' + str_bulan + ' ' + tahun)
}


var tanggal = 31
var bulan = 12
var tahun = 2021

next_date(tanggal, bulan, tahun)

