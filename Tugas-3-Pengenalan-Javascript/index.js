// soal 1
var pertama = "saya sangat senang hari ini"
var kedua = "belajar javascript itu keren"

var string_i = pertama.split(" ")
var string_ii = kedua.split(" ")

// jawaban 1
console.log(`${string_i[0]} ${string_i[2]} ${string_ii[0]} ${string_ii[1].toUpperCase()}`)

// -----------------------------------------------------
// soal 2
var kataPertama = "10"
var kataKedua = "2"
var kataKetiga = "4"
var kataKeempat = "6"

var nKataPertama = parseInt(kataPertama)
var nKataKedua = parseInt(kataKedua)
var nKataKetiga = parseInt(kataKetiga)
var nKataKeempat = parseInt(kataKeempat)

// jawaban 2
console.log((nKataPertama-nKataKetiga) * (nKataKeempat-nKataKedua))

// -----------------------------------------------------
// soal 3
var kalimat = 'wah javascript itu keren sekali'

var kataPertama = kalimat.substring(0, 3)
var kataKedua = kalimat.substring(4, 14)
var kataKetiga = kalimat.substring(15, 18)
var kataKeempat = kalimat.substring(19, 24) 
var kataKelima = kalimat.substring(25, 31)

// jawaban 3
console.log('Kata Pertama: ' + kataPertama)
console.log('Kata Kedua: ' + kataKedua)
console.log('Kata Ketiga: ' + kataKetiga)
console.log('Kata Keempat: ' + kataKeempat)
console.log('Kata Kelima: ' + kataKelima)
