export const UsersComponent = {
    template: `
        <li>
            {{ user.name }} ||
            <button @click="$emit('edit', user.index)">Edit</button>
            <button @click="$emit('delete', user.index)">Delete</button>
        </li>
    `,
    props: ['user', 'index']

}