//soal 1
var nilai = 84

if(nilai >= 85) { console.log('A') }
else if(nilai >= 75 && nilai < 85) { console.log('B') }
else if(nilai >= 65 && nilai < 75) { console.log('C') }
else if(nilai >= 55 && nilai < 65) { console.log('D') }
else { console.log('E') }

//soal 2
var tanggal = 8
var bulan = 8
var tahun = 1998

switch(bulan) {
    case 1: { console.log(tanggal + ' Januari ' + tahun); break }
    case 2: { console.log(tanggal + ' Februari ' + tahun); break }
    case 3: { console.log(tanggal + ' Maret ' + tahun); break }
    case 4: { console.log(tanggal + ' April ' + tahun); break }
    case 5: { console.log(tanggal + ' Mei ' + tahun); break }
    case 6: { console.log(tanggal + ' Juni ' + tahun); break }
    case 7: { console.log(tanggal + ' Juli ' + tahun); break }
    case 8: { console.log(tanggal + ' Agustus ' + tahun); break }
    case 9: { console.log(tanggal + ' September ' + tahun); break }
    case 10: { console.log(tanggal + ' Oktober ' + tahun); break }
    case 11: { console.log(tanggal + ' November ' + tahun); break }
    case 12: { console.log(tanggal + ' Desember ' + tahun); break }
}


//soal 3
var alas = 4
for(var i = 1; i <= alas; i++) {
    var s = ''
    for(var j = 1; j <= i; j++) {
        s += '*'
    }
    console.log(s)
}

//soal 4
var m = 10
var arr = ['I love programming', 'I love Javascript', 'I love VueJs']
var ambil_arr = 0 //pengambilan array
var count = 1 //pembuatan nomor urut
var batas = '' //pembuatan batas


for(var i = 0; i < m; i++) {
    console.log(count + ' - ' + arr[ambil_arr++])  //print array akan berubah dalam setiap perulangan dengan cara ambil_arr++
    if(ambil_arr == arr.length) { ambil_arr = 0 }  //jika nilai ambil_arr melebihi 3, akan di reset menjadi 0, agar tidak undefined 
    
    batas += '='
    
    if(count%3==0){ console.log(batas) } //print batas (=) jika nilai count habis dibagi 3
    count++
}


